<?php

/*
Plugin Name: Kalories
Description: Kalories plugin
Version: 1.0
Text Domain: kalories
Author: Daniele Zerosi
Author URI: http://www.danielezerosi.com
License: GPLv3 or later
*/





if (!defined('ABSPATH'))
	exit;
	
if(!function_exists("is_plugin_active")){
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

include(plugin_dir_path( __FILE__ )."option_page.php");


class kalories_management{

	function __construct(){
		
		add_action('admin_enqueue_scripts', array( $this, 'load_custom_wp_admin_style'), 10);
		add_action('wp_enqueue_scripts', array( $this, 'load_css_and_js' ), 10);
		add_filter( 'plugin_action_links', array( $this, 'add_option_link') , 10, 5 );
		
		add_filter( 'init', array( $this, 'create_custom_post_type') , 10);
		add_filter( 'init', array( $this, 'create_custom_post_type_features') , 10);

	}
	
	function load_custom_wp_admin_style($hook) {
		
		wp_enqueue_script( 'jquery-ui-core', false, array('jquery'));
		wp_enqueue_script( 'jquery-ui-tabs', false, array('jquery','jquery-ui-core'));
		
		wp_register_style( 'kalories_css_backend', plugin_dir_url( __FILE__ ).'css/backend.css', false, '1.0.0' );
		wp_enqueue_style( 'kalories_css_backend' );
	}

	function load_css_and_js($hook) {
		
		wp_enqueue_script( 'jquery-ui-core', false, array('jquery'));
		wp_enqueue_script( 'jquery-ui-datepicker', false, array('jquery'));
		wp_enqueue_style( 'kalories_css_frontend', plugin_dir_url( __FILE__ ).'css/frontend.css');
		wp_enqueue_style( 'jqueryui_css', plugin_dir_url( __FILE__ ).'css/jqueryui.css');
		wp_enqueue_script( 'kalories_js', plugin_dir_url( __FILE__ ).'js/custom_js.js', array('jquery','jquery-ui-core','jquery-ui-datepicker'), '1.0' );
	}	
	
	function add_option_link( $actions, $plugin_file ){
		static $plugin;

		if (!isset($plugin))
			$plugin = plugin_basename(__FILE__);
		if ($plugin == $plugin_file) {
				$settings = array('settings' => '<a href="options-general.php?page=Kalories_Option_Page">' . __('Options', 'kalories') . '</a>');
	    		$actions = array_merge($settings, $actions);	
			}
			
			return $actions;
	}	


	function create_custom_post_type(){
		
		$labels = array(
			'name'                => _x( 'Meals', 'Post Type General Name', 'kalories' ),
			'singular_name'       => _x( 'Meal', 'Post Type Singular Name', 'kalories' ),
			'menu_name'           => __( 'Meals', 'kalories' ),
			'all_items'           => __( 'All Meals', 'kalories' ),
			'view_item'           => __( 'See meal', 'kalories' ),
			'add_new_item'        => __( 'Add meal', 'kalories' ),
			'add_new'             => __( 'Add meal', 'kalories' ),
			'edit_item'           => __( 'Modify meal', 'kalories' ),
			'update_item'         => __( 'Update meal', 'kalories' ),
			'search_items'        => __( 'Search meal', 'kalories' ),
			'not_found'           => __( 'Not found', 'kalories' ),
			'not_found_in_trash'  => __( 'Not found', 'kalories' ),
		);

		$args = array(
			'label'               => __( 'meals', 'kalories' ),
			'description'         => __( 'meals', 'kalories' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'thumbnail'),
			'taxonomies'          => array(),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'menu_icon'           => 'dashicons-carrot',
			'rewrite'			  => array( 'slug' => __("meal_list"), 'with_front' => false ),
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
		);

		register_post_type( 'meals', $args);
		flush_rewrite_rules();

	}

	
	function create_custom_post_type_features(){
		
		$cat_labels = array(
			'singular_name'       => __( 'Meal features', 'kalories' ),
			'all_items'           => __( 'See all', 'kalories' ),
			'add_new_item'        => __( 'Add new feature', 'kalories' ),
		);

		register_taxonomy(
			'meal_features',
			'meals',

			array(
				'label'              => __( 'Meal features', 'kalories' ),
				'hierarchical'       => true,
				'show_ui'            => true,
				'labels'             => $cat_labels,
				'publicly_queryable'  => true,
				'rewrite' => array(
					'slug'          => 'meal_features',
					'with_front'    => true
				)
			)
		);

	}
	
	/*
	Shortcode to generate html and functionalities. No input. 
	Place [generate_meal_list] in a WP page to activate it.
	*/
	static function generate_meal_list($atts){
		
		$code = "";
		
		// delete meal
		if ( isset($_POST["mealId"]) && isset($_POST["deleteMeal"]) && $_POST["deleteMeal"] == true){
			
			$post_id = intval(sanitize_text_field($_POST["mealId"]));
			wp_delete_post( $post_id, true );
		}
		
		// update meal
		if ( isset($_POST["modmealName"]) && isset($_POST["modmealKal"]) && isset($_POST["modmealDate"]) && isset($_POST["mealTimeH"]) && isset($_POST["mealTimeM"]) ){
			
			$meal_id = sanitize_text_field($_POST["mealId"]);
			$meal_name = sanitize_text_field($_POST["modmealName"]);
			$meal_date = sanitize_text_field($_POST["modmealDate"]);
			$meal_kal = intval(sanitize_text_field($_POST["modmealKal"]));
			$meal_h = intval(sanitize_text_field($_POST["mealTimeH"]));
			$meal_m = intval(sanitize_text_field($_POST["mealTimeM"]));
			
			$postdate = $meal_date." ".$meal_h.":".$meal_m.":00";
			
			$my_post = array(
		    	'ID'           => $meal_id,
				'post_title'   => $meal_name,
				'post_content' => $meal_name,
				'post_date' => $postdate
			);

		  wp_update_post( $my_post );
		  wp_publish_post( $meal_id );
		  update_post_meta( $meal_id, "kalories", $meal_kal );
			
		}
		
		// insert meal
		if ( isset($_POST["mealName"]) && isset($_POST["mealKal"]) && isset($_POST["mealDate"]) && isset($_POST["mealTimeH"]) && isset($_POST["mealTimeM"]) ){
			
			$meal_name = sanitize_text_field($_POST["mealName"]);
			$meal_kal = intval(sanitize_text_field($_POST["mealKal"]));
			$meal_date = sanitize_text_field($_POST["mealDate"]);
			$meal_h = intval(sanitize_text_field($_POST["mealTimeH"]));
			$meal_m = intval(sanitize_text_field($_POST["mealTimeM"]));
			
			$postdate = $meal_date." ".$meal_h.":".$meal_m.":00";
			
			if ($meal_kal > 0){
			
			    $array_post = array(
			        'post_content' => $meal_name,
			        'post_title' => $meal_name,
			        'post_status' => 'publish',
			        'post_type' => 'meals',
			        'post_date' => $postdate,
			    );
			    
				$post_id = wp_insert_post( $array_post );
				wp_publish_post( $post_id );
				
				if ($post_id > 0){
					add_post_meta($post_id, "kalories", $meal_kal, false);
				}
			}	
		}
		
		// search form
		
		$code .= "<form action='' class='searchPostForm' method='POST'>";
		$code .= "<table class='meals-table'>";
		$code .= "<tr>";
		$code .= "<td colspan='3'>".__("Search by dates","kalories")."</td>";
		$code .= "</tr>";
		$code .= "<tr>";
		$code .= "<td><input type='text' name='fromDate' id='fromDate' class='required datepicker' placeholder='".__("From date","kalories")."' required /></td>";
		$code .= "<td><input type='text' name='toDate' id='toDate' class='required datepicker' placeholder='".__("To date","kalories")."' required /></td>";
	$code .= "<td>
    			<input type='hidden' name='submitted' id='submitted' value='true' />
   				<button type='submit'>".__("Search","kalories")."</button>
			</td>";
		$code .= "</tr>";
		$code .= "</table>";
		$code .= "</form>";
		
		
		// query to search meals by dates
		
		$single_day = true;
		$days_number = 1;
		$days_text = __("day","kalories");
		
		if ( isset($_POST["fromDate"]) && isset($_POST["toDate"]) ){
			
			$single_day = false;
			
			$from_date = sanitize_text_field($_POST["fromDate"]);
			$to_date = sanitize_text_field($_POST["toDate"]);
			
			$date_start = strtotime($from_date);
			$date_end = strtotime($to_date);
			$datediff = $date_end - $date_start;
			$days_number =  round($datediff / (60 * 60 * 24));

			
			$wpquery = new WP_Query(array(
				'post_type'      => 'meals',
				'posts_per_page' => -1,
				'post_limits' => -1,
				'orderby' => 'publish_date',
				'order' => 'ASC',
			    'date_query' => array(
			        array(
			            'after'     => $from_date,
			            'before'    => $to_date,
			            'inclusive' => true,
			        ),
			    ),
			));
							
		}
		
		
		if($days_number >1){
			$days_text = __("days","kalories");
		}
		
		// query to search today meals
		
		else{
			
			$y = date( 'Y' );
			$m = date( 'm' );
			$d = date( 'd' );
			
			$wpquery = new WP_Query(array(
				'post_type'      => 'meals',
				'posts_per_page' => -1,
				'post_limits' => -1,
				'orderby' => 'ID',
				'order' => 'ASC',
				'date_query' => array(
					array(
						'key'  => $y,
						'value' => $m,
						'day'   => $d,
					),
				),
			));	
		}
		
		// html meals list generation
		
		$code .= "<div class='meals-container'><table class='meals-table'>";
		
		$code .= "<thead><tr>";
		$code .= "<th>".__("Date","kalories")."</th>";
		$code .= "<th>".__("Hours","kalories")."</th>";
		$code .= "<th>".__("Minutes","kalories")."</th>";
		$code .= "<th>".__("Name","kalories")."</th>";
		$code .= "<th>".__("Kalories","kalories")."</th>";
		$code .= "<th>".__("Delete","kalories")."</th>";
		$code .= "<th></th>";
		$code .= "</tr></thead><tbody>";
		
		// backend option value
		$options = get_option( 'Kalories_Option_Page_options' );
		
		if (isset($options["kalories_number"])){
			$option_kal = $options["kalories_number"];
			if ($single_day == false){
				$option_kal = $option_kal * $days_number;
			}
		}
		else{
			$option_kal = 0;
		}
		
		$total_kal = 0;
		
		// generate html depending meals
				
		while ($wpquery->have_posts()): $wpquery->the_post();
			
			$post_id = get_the_ID();
			
			$text = get_the_content();
			$date = get_the_date( 'Y-m-d', $post_id);
			$timeH = get_the_date( 'H', $post_id);
			$timeM = get_the_date( 'i', $post_id);
			$kal = get_post_meta( $post_id, "kalories", true );
			
			$hours = '<select name="mealTimeH" id="mealTimeH" class="required" required>';
			
			for ($i = 0; $i<=24; $i++) {
				if ($i < 10){
					$c = "0".$i;
				}
				else{
					$c = $i;
				}
				$hours .= '<option value="'.$i.'" '.selected( $timeH, $c, false).'>'.$i.'</option>';
			}		
			$hours .= '</select>';
			
			$minutes = '<select name="mealTimeM" id="mealTimeM" class="required" required>';
			
			for ($i = 0; $i<=59; $i++) {
				if ($i < 10){
					$c = "0".$i;
				}
				else{
					$c = $i;
				}
				$minutes .= '<option value="'.$i.'" '.selected( $timeM, $c, false ).'>'.$i.'</option>';
			}		
			$minutes .= '</select>';
			
			$code .= "<form action='' class='primaryPostForm' method='POST'>";
			$code .= "<tr>";
			$code .= "<td><input type='text' name='modmealDate' id='modmealDate' class='required datepicker' value='".$date."' required /></td>";
			$code .= "<td>".$hours."</td>";
			$code .= "<td>".$minutes."</td>";
			$code .= "<td><input type='text' name='modmealName' id='modmealName' class='required' value='".$text."' required /></td>";
			$code .= "<td><input type='text' name='modmealKal' id='modmealKal' class='required' value='".$kal."' required /></td>";
			$code .= "<td><input type='checkbox' id='deleteMeal' name='deleteMeal'></td>";
    		$code .= "<td>
        				<input type='hidden' name='submitted' id='submitted' value='true' />
        				<input type='hidden' name='mealId' id='mealId' value='".$post_id."' />
       					<button type='submit'>".__("Update","kalories")."</button>
    				</td>";
			$code .= "</tr>";
			$code .= "</form>";
			
			$total_kal += $kal;
		
		endwhile;
		
		// check for kal. total number
		if ($total_kal <= $option_kal){
			$color = "green";
		}
		else{
			$color = "red";
		}
		
		// ending html meals list
		
		$code .= "<tr>";
		$code .= "<td></td>";
		$code .= "<td></td>";
		$code .= "<td></td>";
		$code .= "<td><span style='color:".$color."'>".__("Total Kalories:","kalories")." ".$total_kal."</span>";
		$code .= "<br>".__("Max. Kalories:","kalories")." ".$option_kal." (".$days_number." ".$days_text.")</td>";
		$code .= "</tr>";
		
		$code .= "</tbody></table></div>";
		
		// generate html form to insert meals
		
		$hours = '<select name="mealTimeH" id="mealTimeH" class="required" required>';
		for ($i = 0; $i<=24; $i++) {
			$hours .= '<option value="'.$i.'">'.$i.'</option>';
		}		
		$hours .= '</select>';
		
		$minutes = '<select name="mealTimeM" id="mealTimeM" class="required" required>';
		for ($i = 0; $i<=59; $i++) {
			$minutes .= '<option value="'.$i.'">'.$i.'</option>';
		}		
		$minutes .= '</select>';
		
		$code .='<form action="" class="primaryPostForm" method="POST">
    				<fieldset>
        				<label for="mealDate">'.__("Meal date and time","kalories").'</label>
        				<input type="text" name="mealDate" id="mealDate" class="required datepicker" required /> '.__("Time","kalories").' '.$hours.' : '.$minutes.'
    				</fieldset>
    				
    				<fieldset>
        				<label for="mealName">'.__("Meal name","kalories").'</label>
        				<input type="text" name="mealName" id="mealName" class="required" required />
    				</fieldset>
 
				    <fieldset>
				        <label for="mealKal">'.__("Meal Kalories","kalories").'</label>
				        <input type="text" name="mealKal" id="mealKal" class="required" required/>
				    </fieldset>
    				<fieldset>
        				<input type="hidden" name="submitted" id="submitted" value="true" />
       					<button type="submit">'.__("Insert","kalories").'</button>
    				</fieldset>
				</form>';

		return $code;
		
	}
	
}

$kalories_management = new kalories_management();

add_shortcode( 'generate_meal_list', array( 'kalories_management', 'generate_meal_list' ) );





