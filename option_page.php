<?php

class MySettingsPage{

    private $options;

    public function __construct(){
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
        
    }

    public function add_plugin_page(){
        // This page will be under "Settings"
        add_options_page(
            __("Kalories Options","kalories"), 
            __("Kalories Options","kalories"), 
            'manage_options', 
            'Kalories_Option_Page', 
            array( $this, 'create_admin_page' )
        );
    }

    public function create_admin_page(){
        // Set class property
        $this->options = get_option( 'Kalories_Option_Page_options' );
        ?>
        <div class="wrap" id="tabs">
            <?php echo __("<h1>Kalories Options Page</h1>", "kalories"); ?>
            <br><br>
            <form method="post" action="options.php">
	            <div id="tabs_options">
					<ul>
					
					    <li><a href="#tab_kalories" class="nav-tab"><?php _e("Kalories options","kalories"); ?></a></li>
						
					</ul>
					
		            <?php
		                settings_fields( 'Kalories_option_group' );
		                do_settings_sections( 'Kalories_Option_Page' );
		                submit_button();
		            ?>
		            
			    </div>
            </form>
        </div>
        <?php
    }


    public function page_init(){        
        register_setting(
            'Kalories_option_group', // Option group
            'Kalories_Option_Page_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

		add_settings_section(
			'setting_section_1', // ID
			"", // Title
			array( $this, 'print_section_info_kalories' ), // Callback
			'Kalories_Option_Page' // Page
		);
	
		add_settings_section(
			'setting_section_1_close', // ID
			"", // Title
			array( $this, 'print_section_info_kalories_close' ), // Callback
			'Kalories_Option_Page' // Page
		);
			
		add_settings_field(
			'kalories_number', // ID
			'Kalories number', // Title 
			array( $this, 'kalories_number_callback' ), // Callback
			'Kalories_Option_Page', // Page
			'setting_section_1' // Section           
		);

    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input ){
        $new_input = array();
        if( isset( $input['kalories_number'] ) )
            $new_input['kalories_number'] = sanitize_text_field( $input['kalories_number'] );
        return $new_input;
    }
    
    /** 
     * Print the Section text
     */
    public function print_section_info_kalories(){
        echo "<div id='tab_kalories'>";
    }
    
    public function print_section_info_kalories_close(){
        echo "</div>";
    }


    /** 
     * Get the settings option array and print one of its values
     */
    public function kalories_number_callback(){
    	
	        printf('<p><label><input type="text" id="kalories_number" name="Kalories_Option_Page_options[kalories_number]" value="%s" />',
	        isset( $this->options['kalories_number'] ) ? esc_attr( $this->options['kalories_number']) : '');

	        echo __("<br><strong>Insert Kalories number", "kalories");
    }
	
}


if( is_admin() ){
	$my_settings_page = new MySettingsPage();
}
